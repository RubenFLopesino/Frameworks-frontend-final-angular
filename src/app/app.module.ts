import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HomeContainer } from './core/home/home.container';
import { TokenInterceptor } from './helpers/auth.interceptor';
import { LoginModule } from './login/login.module';

@NgModule({
  declarations: [AppComponent, HomeContainer],
  imports: [BrowserModule, AppRoutingModule, CoreModule, LoginModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
