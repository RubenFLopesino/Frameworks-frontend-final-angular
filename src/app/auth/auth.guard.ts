import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../core/models/user.model';
import { TokenStorageService } from '../login/services/tokenStorage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router
  ) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    const token = this.tokenStorageService.getToken();
    let user;
    let canActivate: boolean = false;
    if (token)
      user = this.tokenStorageService.getDecodedAccessToken(token) as User;
    if (user) {
      if (user.isAdmin) canActivate = true;
      else {
        canActivate = false;
      }
    }
    if (!canActivate) {
      this.router.navigate(['acceso-denegado']);
    }
    return canActivate;
  }
}
