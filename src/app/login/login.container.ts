import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.container.html',
  styleUrls: ['./login.container.scss'],
})
export class LoginContainer implements OnInit {
  isRegister: boolean = false;
  constructor(private router: Router) {}
  ngOnInit(): void {
    if (this.router.url.includes('registro')) {
      this.isRegister = true;
    }
  }
}
