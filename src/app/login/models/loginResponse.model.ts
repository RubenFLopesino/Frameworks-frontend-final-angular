export interface LoginResponse {
  id: string;
  email: string;
  role: string;
  accessToken: string;
}
