import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { catchError } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { TokenStorageService } from '../../services/tokenStorage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  form!: FormGroup;
  loginError: boolean = false;
  isLoggedIn: boolean = false;

  constructor(
    private readonly fb: FormBuilder,
    private loginService: LoginService,
    private tokenStorage: TokenStorageService,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.router.navigate(['/']);
    }
  }

  submitForm() {
    if (this.form.valid) {
      const loginData = this.form.getRawValue();
      this.loginService
        .login(loginData.email, loginData.password)
        .pipe(
          catchError(async (error) => {
            console.error(error);
          })
        )
        .subscribe((result) => {
          if (result) {
            this.tokenStorage.saveToken(result.accessToken);
            this.loginError = false;
            this.isLoggedIn = true;
            this.router.navigate(['/']);
          } else {
            this.loginError = true;
            this.isLoggedIn = false;
          }
        });
    } else {
      console.log('There is a problem with the form');
    }
  }
}
