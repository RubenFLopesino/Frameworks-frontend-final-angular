import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { TokenStorageService } from '../../services/tokenStorage.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  form!: FormGroup;
  registerError: boolean = false;
  isLoggedIn: boolean = false;
  constructor(
    private readonly fb: FormBuilder,
    private loginService: LoginService,
    private tokenStorage: TokenStorageService,
    private router: Router
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.router.navigate(['/']);
    }
  }

  submitForm() {
    if (this.form.valid) {
      const registerData = this.form.getRawValue();
      this.loginService
        .register(registerData.name, registerData.email, registerData.password)
        .pipe(
          catchError(async (error) => {
            console.error(error);
          })
        )
        .subscribe((result) => {
          if (result) {
            this.registerError = false;
            this.loginService
              .login(registerData.email, registerData.password)
              .subscribe((loginResult) => {
                this.tokenStorage.saveToken(loginResult.accessToken);
                window.location.replace('/');
              });
          } else {
            this.registerError = true;
          }
        });
    } else {
      console.log('There is a problem with the form');
    }
  }
}
