import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { User } from 'src/app/core/models/user.model';
const TOKEN_KEY = 'auth-token';
@Injectable({
  providedIn: 'root',
})
export class TokenStorageService {
  constructor() {}
  signOut(): void {
    window.sessionStorage.clear();
  }
  saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }
  getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  getDecodedAccessToken(token: string): User | null {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
}
