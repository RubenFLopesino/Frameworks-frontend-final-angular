import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeContainer } from './core/home/home.container';
import { LoginContainer } from './login/login.container';
import { ForbiddenAccessComponent } from './core/forbidden-access/forbidden-access.component';
import { AuthGuard } from './auth/auth.guard';
import { AdminComponent } from './core/admin/admin.component';
import { HomeComponent } from './core/admin/zones/home/home.component';
import { CreateCategoryComponent } from './core/admin/zones/create-category/create-category.component';
import { CreateProductComponent } from './core/admin/zones/create-product/create-product.component';
import { StockManagementComponent } from './core/admin/zones/stock-management/stock-management.component';
const routes: Routes = [
  {
    path: '',
    component: HomeContainer,
  },
  {
    path: 'login',
    component: LoginContainer,
  },
  {
    path: 'registro',
    component: LoginContainer,
  },
  {
    path: 'acceso-denegado',
    component: ForbiddenAccessComponent,
  },
  {
    path: 'zona-administrador',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeComponent,
      },

      {
        path: 'crear-categoria',
        component: CreateCategoryComponent,
      },

      {
        path: 'crear-producto',
        component: CreateProductComponent,
      },
      {
        path: 'editar-stocks',
        component: StockManagementComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
