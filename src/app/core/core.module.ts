import { NgModule } from '@angular/core';
import { NavbarModule } from './navbar/navbar.module';
import { CartContainer } from './cart/cart.container';
import { DetailBarComponent } from './cart/components/detail-bar/detail-bar.component';
import { CartProductDetailComponent } from './cart/components/cart-product-detail/cart-product-detail.component';
import { BrowserModule } from '@angular/platform-browser';
import { ForbiddenAccessComponent } from './forbidden-access/forbidden-access.component';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './admin/zones/home/home.component';
import { CreateProductComponent } from './admin/zones/create-product/create-product.component';
import { CreateCategoryComponent } from './admin/zones/create-category/create-category.component';
import { AdminMenuComponent } from './admin/zones/home/components/admin-menu/admin-menu.component';
import { CategoryFormComponent } from './admin/zones/create-category/components/category-form/category-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductFormComponent } from './admin/zones/create-product/components/product-form/product-form.component';
import { FeaturedCategoriesContainer } from './home/components/featured-categories/featured-categories.container';
import { CategoryCardComponent } from './home/components/featured-categories/components/category-card/category-card.component';
import { ProductCardComponent } from './home/components/products-list/components/product-card/product-card.component';
import { ProductsListContainer } from './home/components/products-list/products-list.container';
import { PaginationComponent } from './home/components/products-list/components/pagination/pagination.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AllCategoriesComponent } from './home/components/all-categories/all-categories.component';
import { MenuComponent } from './home/components/all-categories/components/menu/menu.component';
import { CategoryListComponent } from './home/components/all-categories/components/category-list/category-list.component';
import { CategoryButtonComponent } from './home/components/all-categories/components/category-list/components/category-button/category-button.component';
import { ProductDetailComponent } from './home/components/product-detail/product-detail.component';
import { FinishPurchaseModalComponent } from './home/components/finish-purchase-modal/finish-purchase-modal.component';
import { StockManagementComponent } from './admin/zones/stock-management/stock-management.component';

@NgModule({
  imports: [
    NavbarModule,
    BrowserModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  exports: [
    NavbarModule,
    CartContainer,
    FeaturedCategoriesContainer,
    ProductsListContainer,
    AllCategoriesComponent,
    ProductDetailComponent,
    FinishPurchaseModalComponent,
  ],
  providers: [],
  declarations: [
    FeaturedCategoriesContainer,
    CategoryCardComponent,
    PaginationComponent,
    ProductsListContainer,
    ProductCardComponent,
    CartContainer,
    DetailBarComponent,
    CartProductDetailComponent,
    ForbiddenAccessComponent,
    AdminComponent,
    HomeComponent,
    CreateProductComponent,
    CreateCategoryComponent,
    AdminMenuComponent,
    CategoryFormComponent,
    ProductFormComponent,
    AllCategoriesComponent,
    MenuComponent,
    CategoryListComponent,
    CategoryButtonComponent,
    ProductDetailComponent,
    FinishPurchaseModalComponent,
    StockManagementComponent,
  ],
})
export class CoreModule {}
