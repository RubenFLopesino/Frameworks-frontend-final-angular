import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-detail-bar',
  templateUrl: './detail-bar.component.html',
  styleUrls: ['./detail-bar.component.scss'],
})
export class DetailBarComponent implements OnInit {
  @Output() showingCart: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() totalPrice: number = 0.0;

  constructor() {}

  ngOnInit(): void {}
}
