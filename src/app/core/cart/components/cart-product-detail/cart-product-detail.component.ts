import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  CartButtonAction,
  CartButtonActions,
  ProductToCart,
} from 'src/app/core/models/cart.model';

@Component({
  selector: 'app-cart-product-detail',
  templateUrl: './cart-product-detail.component.html',
  styleUrls: ['./cart-product-detail.component.scss'],
})
export class CartProductDetailComponent implements OnInit {
  @Input() product!: ProductToCart;
  @Output() action: EventEmitter<CartButtonAction> =
    new EventEmitter<CartButtonAction>();

  constructor() {}

  ngOnInit(): void {}

  addOneToCart() {
    this.action.emit({
      product: this.product.product,
      action: CartButtonActions.addOne,
    });
  }
  substractOneFromCart() {
    this.action.emit({
      product: this.product.product,
      action: CartButtonActions.substractOne,
    });
  }
  deleteFromCart() {
    this.action.emit({
      product: this.product.product,
      action: CartButtonActions.delete,
    });
  }
}
