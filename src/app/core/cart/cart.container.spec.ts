import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartContainer } from './cart.container';

describe('CartContainer', () => {
  let component: CartContainer;
  let fixture: ComponentFixture<CartContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CartContainer],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
