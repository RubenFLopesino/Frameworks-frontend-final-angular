import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Cart } from '../../models/cart.model';
import { TokenStorageService } from 'src/app/login/services/tokenStorage.service';

const API_URL = `${environment.apiBaseUrl}/cart`;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class CartService {
  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorageService
  ) {}

  getById(id: string): Observable<Cart> {
    return this.http.get<Cart>(`${API_URL}/${id}`, httpOptions);
  }

  getByUserId(userId: string): Observable<Cart> {
    return this.http.get<Cart>(`${API_URL}/byUser/${userId}`, httpOptions);
  }

  saveCart(cart: Cart): void {
    this.http.post<any>(`${API_URL}`, cart, httpOptions).subscribe((result) => {
      this.tokenStorage.saveToken(result.accessToken);
    });
  }

  finishPurchase(cart: Cart): void {
    this.http
      .put<any>(`${API_URL}/finishPurchase`, cart, httpOptions)
      .subscribe();
  }
}
