import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Cart, CartButtonAction } from '../models/cart.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.container.html',
  styleUrls: ['./cart.container.scss'],
  animations: [
    trigger('showCart', [
      state(
        'show',
        style({
          opacity: 1,
        })
      ),
      state(
        'hide',
        style({
          opacity: 0,
          'z-index': -1,
        })
      ),
      transition('show => hide', [animate('0.2s')]),
      transition('hide => show', [animate('0.2s')]),
    ]),
  ],
})
export class CartContainer {
  @Input() showingCart: boolean = false;
  @Input() cart!: Cart;
  @Input() totalPrice: number = 0;
  @Input() isLoggedIn: boolean = false;
  @Output() isCartHided: EventEmitter<boolean> = new EventEmitter<boolean>(
    true
  );
  @Output() cartAction: EventEmitter<CartButtonAction> =
    new EventEmitter<CartButtonAction>();
  @Output() cartSaved: EventEmitter<Cart> = new EventEmitter<Cart>();
  @Output() finishPurchase: EventEmitter<MouseEvent> =
    new EventEmitter<MouseEvent>();

  constructor() {}

  closeCart() {
    this.showingCart = false;
    this.isCartHided.emit(this.showingCart);
  }
}
