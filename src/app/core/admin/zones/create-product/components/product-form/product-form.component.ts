import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/core/models/category.model';
import { Product } from 'src/app/core/models/product.model';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit {
  form!: FormGroup;
  categories: Category[] = [];
  constructor(
    private readonly fb: FormBuilder,
    private categoriesService: CategoriesService,
    private productsService: ProductsService,
    private router: Router
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: ['', [Validators.required]],
      image: ['', [Validators.required]],
      price: ['', [Validators.required]],
      stock: ['', [Validators.required]],
      category: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.categoriesService.getAll().subscribe((categories) => {
      this.categories = categories;
    });
  }

  convertToBase64(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.form.patchValue({ image: reader.result });
    };
  }

  submitForm() {
    this.productsService
      .create(this.form.getRawValue() as Product)
      .subscribe((result) => {
        if (result.id) {
          window.alert('Producto creado correctamente');
          this.router.navigate(['/zona-administrador']);
        }
      });
  }
}
