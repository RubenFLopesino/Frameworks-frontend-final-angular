import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/core/models/category.model';
import { CategoriesService } from 'src/app/core/services/categories.service';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss'],
})
export class CategoryFormComponent implements OnInit {
  form!: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private categoriesService: CategoriesService,
    private router: Router
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      image: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {}

  convertToBase64(event: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.form.patchValue({ image: reader.result });
    };
  }

  submitForm() {
    if (this.form.valid) {
      this.categoriesService
        .create(this.form.getRawValue() as Category)
        .subscribe((result) => {
          if (result.id) {
            window.alert('Categoría creada correctamente');
            this.router.navigate(['/zona-administrador']);
          }
        });
    } else {
      console.log('There is a problem with the form');
    }
  }
}
