import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/core/models/product.model';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-stock-management',
  templateUrl: './stock-management.component.html',
  styleUrls: ['./stock-management.component.scss'],
})
export class StockManagementComponent implements OnInit {
  products: Product[] = [];
  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {
    this.productsService
      .getAll()
      .subscribe((result) => (this.products = result));
  }

  updateStock(event: any, product: Product) {
    this.productsService
      .updateStock(product.id, event.target.value)
      .subscribe();
  }
}
