import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from 'src/app/core/models/category.model';

const API_URL = `${environment.apiBaseUrl}/categories`;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Category[]> {
    return this.http.get<Category[]>(API_URL, httpOptions);
  }

  getById(id: string): Observable<Category> {
    return this.http.get<Category>(`${API_URL}/${id}`, httpOptions);
  }

  create(category: Category): Observable<Category> {
    return this.http.post<Category>(`${API_URL}`, category, httpOptions);
  }
}
