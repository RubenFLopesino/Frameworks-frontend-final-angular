import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from 'src/app/core/models/product.model';

const API_URL = `${environment.apiBaseUrl}/products`;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(API_URL, httpOptions);
  }

  create(product: Product): Observable<Product> {
    return this.http.post<Product>(`${API_URL}`, product, httpOptions);
  }

  updateStock(id: string, stock: number): Observable<Product> {
    return this.http.put<Product>(
      `${API_URL}/updateStock/${id}`,
      { stock },
      httpOptions
    );
  }
}
