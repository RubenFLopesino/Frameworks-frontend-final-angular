import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavbarContainer } from './navbar.container';

@NgModule({
  declarations: [NavbarContainer],
  imports: [CommonModule, RouterModule],
  exports: [NavbarContainer],
})
export class NavbarModule {}
