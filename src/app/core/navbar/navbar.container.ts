import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TokenStorageService } from 'src/app/login/services/tokenStorage.service';

@Component({
  selector: 'gc-navbar',
  templateUrl: './navbar.container.html',
  styleUrls: ['./navbar.container.scss'],
})
export class NavbarContainer implements OnInit {
  @Input() isLoggedIn: boolean = false;
  @Input() isAdmin: boolean = false;
  @Input() showCartButton: boolean = false;
  @Input() isAdminZone: boolean = false;
  @Input() isAdminHome: boolean = true;
  @Output() showCartEmitter: EventEmitter<MouseEvent> =
    new EventEmitter<MouseEvent>();

  constructor(private tokenStorage: TokenStorageService) {}

  ngOnInit(): void {}

  logOut() {
    this.tokenStorage.signOut();
    window.location.replace('/');
  }
  showCart(event: MouseEvent) {
    event.preventDefault();
    this.showCartEmitter.emit(event);
  }
}
