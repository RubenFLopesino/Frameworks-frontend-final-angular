import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/login/services/tokenStorage.service';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { ProductsService } from 'src/app/core/services/products.service';
import { CartService } from '../cart/services/cart.service';
import {
  Cart,
  CartButtonAction,
  CartButtonActions,
} from '../models/cart.model';
import { Category } from '../models/category.model';
import { Product } from '../models/product.model';
import { User } from '../models/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.container.html',
  styleUrls: ['./home.container.scss'],
})
export class HomeContainer implements OnInit {
  isLoggedIn: boolean = false;
  isAdmin: boolean = false;
  showingCart: boolean = false;
  showingCategories: boolean = false;
  showProductDetail: boolean = false;
  showFinishPurchase: boolean = false;
  showedProduct!: Product;
  categories: Category[] = [];
  featuredCategories: Category[] = [];
  allProducts: Product[] = [];
  productsToShow: Product[] = [];
  totalPages!: number;
  user!: User | null;
  cart: Cart = {
    id: null,
    totalPrice: 0.0,
    userId: null,
    isActive: true,
    productsToCart: [],
  };
  productAddedToCart!: Product;
  readonly pageLength: number = 12;
  //Comment to test pipeline

  constructor(
    private tokenStorage: TokenStorageService,
    private categoriesService: CategoriesService,
    private productsService: ProductsService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.categoriesService.getAll().subscribe((result) => {
      this.categories = result;
      this.featuredCategories = result.slice(0, 4);
    });

    this.productsService.getAll().subscribe((result) => {
      this.allProducts = result;
      this.productsToShow = result;
      this.totalPages = Math.ceil(this.productsToShow.length / this.pageLength);
      const token = this.tokenStorage.getToken();
      if (token) {
        this.user = this.tokenStorage.getDecodedAccessToken(token);
        if (this.user) {
          this.isAdmin = this.user.isAdmin;
          this.loadDataFromToken(this.user);
        }
      }
    });
  }

  private loadDataFromToken(user: User) {
    this.isLoggedIn = true;
    this.cart.userId = user.id ?? null;
    if (user.cart) {
      this.cartService.getById(user.cart).subscribe((cart) => {
        this.cart.id = cart.id;
        this.cart.productsToCart = cart.productsToCart;
        this.cart.totalPrice = +cart.totalPrice;
        this.cart.productsToCart.forEach((productToCart) => {
          const productIndex = this.allProducts.findIndex(
            (product) => product.id === productToCart.product.id
          );
          this.allProducts[productIndex].stock -= productToCart.quantity;
        });
      });
    }
  }

  finishPurchase() {
    this.showingCart = false;
    this.showFinishPurchase = true;
  }

  async closePurchaseModal() {
    this.showFinishPurchase = false;
    this.cartService.finishPurchase(this.cart);
    await this.delay(1000);
    this.cart = {
      id: null,
      totalPrice: 0.0,
      userId: null,
      isActive: true,
      productsToCart: [],
    };
  }

  async addProductToCart(product: Product) {
    let productIndex = this.cart.productsToCart.findIndex(
      (cartProduct) => cartProduct.product.id === product.id
    );
    if (productIndex >= 0) {
      this.cart.productsToCart[productIndex].quantity += 1;
    } else {
      this.cart.productsToCart.push({
        product,
        quantity: 1,
        id: null,
      });
    }
    this.cart.totalPrice = +this.cart.totalPrice + +product.price;
    productIndex = this.productsToShow.findIndex(
      (productsToShow) => productsToShow.id === product.id
    );
    if (productIndex >= 0) this.productsToShow[productIndex].stock -= 1;
    if (this.isLoggedIn && !this.isAdmin) this.saveCart(this.cart);
    if (!this.showingCart) this.showingCart = true;
    await this.delay(1500);
    this.showingCart = false;
  }

  showCart() {
    this.showingCart = !this.showingCart;
  }

  toggleCategories(event: boolean) {
    this.showingCategories = event;
  }

  hidedFromCart() {
    this.showingCart = false;
  }

  cartAction(action: CartButtonAction) {
    let productIndex = -1;
    switch (action.action) {
      case CartButtonActions.addOne:
        productIndex = this.allProducts.findIndex(
          (product) => product.id === action.product.id
        );
        if (this.allProducts[productIndex].stock != 0) {
          this.allProducts[productIndex].stock -= 1;
          productIndex = this.cart.productsToCart.findIndex(
            (cartProduct) => cartProduct.product.id === action.product.id
          );
          this.cart.productsToCart[productIndex].quantity += 1;
          this.cart.totalPrice = +this.cart.totalPrice + +action.product.price;
        }
        break;
      case CartButtonActions.substractOne:
        productIndex = this.allProducts.findIndex(
          (product) => product.id === action.product.id
        );
        this.allProducts[productIndex].stock += 1;
        productIndex = this.cart.productsToCart.findIndex(
          (cartProduct) => cartProduct.product.id === action.product.id
        );
        this.cart.productsToCart[productIndex].quantity -= 1;
        this.cart.totalPrice = +this.cart.totalPrice - +action.product.price;
        if (this.cart.productsToCart[productIndex].quantity === 0)
          this.cart.productsToCart.splice(productIndex, 1);

        break;
      case CartButtonActions.delete:
        this.deleteFromCart(action.product);
        break;
    }
    if (this.isLoggedIn && !this.isAdmin) this.saveCart(this.cart);
  }

  private deleteFromCart(product: Product) {
    let productIndex = this.cart.productsToCart.findIndex(
      (cartProduct) => cartProduct.product.id === product.id
    );
    const cartQuantity = this.cart.productsToCart[productIndex].quantity;
    this.cart.totalPrice =
      +this.cart.totalPrice - +product.price * cartQuantity;
    this.cart.totalPrice = +this.cart.totalPrice < 0 ? 0 : this.cart.totalPrice;

    this.cart.productsToCart.splice(productIndex, 1);
    productIndex = this.allProducts.findIndex(
      (originalProduct) => originalProduct.id === product.id
    );
    this.allProducts[productIndex].stock += cartQuantity;
  }

  private delay(miliseconds: number) {
    return new Promise((resolve) => setTimeout(resolve, miliseconds));
  }

  categoryClicked(category: Category) {
    this.productsToShow = this.allProducts.filter(
      (product) => product.category.id === category.id
    );
    this.totalPages = Math.ceil(this.productsToShow.length / this.pageLength);
    if (this.showingCategories) this.showingCategories = false;
  }

  deleteCategoryFilter() {
    this.productsToShow = this.allProducts;
    this.totalPages = Math.ceil(this.productsToShow.length / this.pageLength);
  }

  toggleProductDetail(product?: Product) {
    if (product) {
      this.showedProduct = product;
      this.showProductDetail = true;
    } else {
      this.showProductDetail = false;
    }
  }
  private saveCart(cart: Cart) {
    this.cartService.saveCart(cart);
  }
}
