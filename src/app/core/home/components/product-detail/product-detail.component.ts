import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from 'src/app/core/models/product.model';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  animations: [
    trigger('showDetail', [
      state(
        'show',
        style({
          opacity: 1,
        })
      ),
      state(
        'hide',
        style({
          opacity: 0,
          'z-index': -1,
        })
      ),
      transition('show => hide', [animate('0.2s')]),
      transition('hide => show', [animate('0.2s')]),
    ]),
  ],
})
export class ProductDetailComponent {
  @Input() product!: Product;
  @Input() showDetail: boolean = false;
  @Output() closeDetail: EventEmitter<MouseEvent> =
    new EventEmitter<MouseEvent>();

  constructor() {}
}
