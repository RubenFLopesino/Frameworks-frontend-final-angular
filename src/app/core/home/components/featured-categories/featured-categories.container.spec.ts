import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedCategoriesContainer } from './featured-categories.container';

describe('FeaturedCategoriesContainer', () => {
  let component: FeaturedCategoriesContainer;
  let fixture: ComponentFixture<FeaturedCategoriesContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FeaturedCategoriesContainer],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedCategoriesContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
