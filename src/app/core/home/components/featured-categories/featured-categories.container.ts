import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/app/core/models/category.model';

@Component({
  selector: 'app-featured-categories',
  templateUrl: './featured-categories.container.html',
  styleUrls: ['./featured-categories.container.scss'],
})
export class FeaturedCategoriesContainer implements OnInit {
  @Input() categories: Category[] = [];
  @Output() categoryClicked: EventEmitter<Category> =
    new EventEmitter<Category>();
  @Output() showAllCategories: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  constructor() {}

  ngOnInit(): void {}
}
