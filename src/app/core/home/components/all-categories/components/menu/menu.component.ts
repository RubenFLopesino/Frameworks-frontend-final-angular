import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  @Output() closeCategories: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  constructor() {}

  ngOnInit(): void {}
  close() {
    this.closeCategories.emit(false);
  }
}
