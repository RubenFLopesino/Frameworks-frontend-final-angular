import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category } from 'src/app/core/models/category.model';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
})
export class CategoryListComponent {
  @Input() categories: Category[] = [];
  @Output() categoryFilter: EventEmitter<Category> =
    new EventEmitter<Category>();

  constructor() {}
}
