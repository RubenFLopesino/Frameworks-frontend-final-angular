import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/app/core/models/category.model';

@Component({
  selector: 'app-category-button',
  templateUrl: './category-button.component.html',
  styleUrls: ['./category-button.component.scss'],
})
export class CategoryButtonComponent implements OnInit {
  @Input() category!: Category;
  @Output() categoryFilter: EventEmitter<Category> =
    new EventEmitter<Category>();

  constructor() {}

  ngOnInit(): void {}
}
