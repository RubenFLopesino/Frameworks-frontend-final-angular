import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { Category } from 'src/app/core/models/category.model';
@Component({
  selector: 'app-all-categories',
  templateUrl: './all-categories.component.html',
  styleUrls: ['./all-categories.component.scss'],
  animations: [
    trigger('showCategories', [
      state(
        'show',
        style({
          opacity: 1,
        })
      ),
      state(
        'hide',
        style({
          opacity: 0,
          'z-index': -1,
        })
      ),
      transition('show => hide', [animate('0.2s')]),
      transition('hide => show', [animate('0.2s')]),
    ]),
  ],
})
export class AllCategoriesComponent {
  @Input() showingCategories: boolean = false;
  @Input() categories: Category[] = [];
  @Output() closedMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() filterCategory: EventEmitter<Category> =
    new EventEmitter<Category>();

  constructor() {}
}
