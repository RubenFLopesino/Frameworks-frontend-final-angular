import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Product } from 'src/app/core/models/product.model';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.container.html',
  styleUrls: ['./products-list.container.scss'],
  animations: [
    trigger('changingPage', [
      state(
        'loaded',
        style({
          opacity: 1,
        })
      ),
      state(
        'loading',
        style({
          opacity: 0,
        })
      ),
      transition('loaded => loading', [animate('0.3s')]),
      transition('loading => loaded', [animate('0.3s')]),
    ]),
  ],
})
export class ProductsListContainer implements OnChanges {
  @Input() products: Product[] = [];
  @Input() totalPages!: number;
  @Input() pageLength!: number;
  @Output() productAddedToCart: EventEmitter<Product> =
    new EventEmitter<Product>();
  @Output() showProduct: EventEmitter<Product> = new EventEmitter<Product>();
  pageProducts: Product[] = [];
  currentPage: number = 1;
  isChanging = false;

  constructor() {}

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    if (
      changes['products'].currentValue !== changes['products'].previousValue
    ) {
      await this.getProductsPage(this.currentPage);
    }
  }

  async goTo(page: number): Promise<void> {
    this.currentPage = page;

    await this.getProductsPage(this.currentPage);
  }

  async nextPage(page: number): Promise<void> {
    this.currentPage = page + 1;

    await this.getProductsPage(this.currentPage);
  }

  async previousPage(page: number): Promise<void> {
    this.currentPage = page - 1;

    await this.getProductsPage(this.currentPage);
  }

  private async getProductsPage(page: number) {
    this.toggle();
    await this.delay(400);
    this.pageProducts = this.products.slice(
      (page - 1) * Math.floor(this.pageLength),
      page * Math.floor(this.pageLength)
    );
    this.toggle();
  }

  private toggle() {
    this.isChanging = !this.isChanging;
  }

  private delay(miliseconds: number) {
    return new Promise((resolve) => setTimeout(resolve, miliseconds));
  }
}
