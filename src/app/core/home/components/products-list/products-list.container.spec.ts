import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsListContainer } from './products-list.container';

describe('ProductsListContainer', () => {
  let component: ProductsListContainer;
  let fixture: ComponentFixture<ProductsListContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductsListContainer],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsListContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
