import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/core/models/product.model';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() product!: Product;
  @Output() productAddedToCart: EventEmitter<Product> =
    new EventEmitter<Product>();
  constructor() {}
  @Output() showProduct: EventEmitter<Product> = new EventEmitter<Product>();

  ngOnInit(): void {}
}
