import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { User } from 'src/app/core/models/user.model';
@Component({
  selector: 'app-finish-purchase-modal',
  templateUrl: './finish-purchase-modal.component.html',
  styleUrls: ['./finish-purchase-modal.component.scss'],
  animations: [
    trigger('showModal', [
      state(
        'show',
        style({
          opacity: 1,
        })
      ),
      state(
        'hide',
        style({
          opacity: 0,
          'z-index': -1,
        })
      ),
      transition('show => hide', [animate('0.2s')]),
      transition('hide => show', [animate('0.2s')]),
    ]),
  ],
})
export class FinishPurchaseModalComponent {
  @Input() price: number = 0;
  @Input() user!: User | null;
  @Input() showModal: boolean = false;
  @Output() closeModal: EventEmitter<MouseEvent> =
    new EventEmitter<MouseEvent>();

  constructor() {}
}
