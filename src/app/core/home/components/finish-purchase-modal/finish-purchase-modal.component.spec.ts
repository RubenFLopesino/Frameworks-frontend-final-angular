import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishPurchaseModalComponent } from './finish-purchase-modal.component';

describe('FinishPurchaseModalComponent', () => {
  let component: FinishPurchaseModalComponent;
  let fixture: ComponentFixture<FinishPurchaseModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishPurchaseModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishPurchaseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
