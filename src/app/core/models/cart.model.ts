import { Product } from './product.model';

export enum CartButtonActions {
  addOne,
  substractOne,
  delete,
}
export interface Cart {
  id: string | null;
  totalPrice: number;
  isActive: boolean;
  userId: string | null;
  productsToCart: ProductToCart[];
}

export interface ProductToCart {
  id: string | null;
  quantity: number;
  product: Product;
}

export interface CartButtonAction {
  product: Product;
  action: CartButtonActions;
}
