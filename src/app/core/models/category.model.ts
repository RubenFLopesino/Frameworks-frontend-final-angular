import { Product } from './product.model';

export interface Category {
  id: string | null;
  name: string;
  image: string;
  products: Product[];
}
