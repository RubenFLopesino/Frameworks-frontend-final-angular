export interface User {
  id?: string;
  email?: string;
  password?: string;
  isAdmin: boolean;
  cart: string;
  name: string;
}
